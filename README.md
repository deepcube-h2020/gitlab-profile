# Description

Fore more information about DeepCube project please click  [here](https://deepcube-h2020.eu//) .

# Git Organization

Here you have the final repository of DeepCube platform

>> Legacy repository : https://gitlab.com/deepcube-draft
>> Still containing some docker images from "Docker Registry" 

## Sections

The code is organized in tree sections

### DeepCube Platform Technology

Which contains the software platform / framework

### DeepCube Usecases

Alls usecases have their own git repositories on Gitlab or Github, to avoid multiple external link repositories

This repository  [here](https://gitlab.com/deepcube-h2020/usecases) , aggregated alls usecases by mirroring these.

### DeepCube Technological Component

In this section you will find all necessary to instanciate your own instance onto a public cloud.
For now CREODIAS (CloudFerro) and OVH is supported, this is an open source project feel free to contribute and add more "public cloud" provider


## Content

Each project, has been segmented in two parts 

### Source Code

Containing source code of technological component, and associated Dockerfile.


### Deployment Code

All necessary :

- to provision cloud resources through Terraform or OpenTofu.
- to ensure apps & configuration are well deployed through Ansible.
- With two approach SWARM cluster & Kubernetes (in order to mutualize resources, we recommended to use Kubernetes provided by Hopsworks)
